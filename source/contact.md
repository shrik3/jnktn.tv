# Contact

Email is the best way to get in contact with us, since the inbox is actively monitored. See below for our addresses. Also find a list of our social media accounts that you can follow us on for the weekly schedule as well as other updates from the project.

## Email
We have two email addresses with different purposes. To help prevent spam, we have removed the '@' and '.' (replace these when using).

<p class="subheader">info[at]jnktn[dot]tv</p>
This email address can be used for general enquiries, suggestions and feedback.

<p class="subheader">shows[at]jnktn[dot]tv</p>
If you'd like to enquire about your own show or slot with us, please use this address.

## Social Media

### Mastodon

<h3 class="link"><a href="https://mastodon.online/@jnktn_tv" rel="me noopener" target="_blank">@jnktn_tv</a>
</h3>

### Instagram

<h3 class="link"><a href="https://www.instagram.com/jnktn.tv/" rel="me noopener"
        target="_blank">@jnktn.tv</a></h3>

### Twitter

<h3 class="link"><a href="https://twitter.com/jnktn_tv" rel="me noopener" target="_blank">@jnktn_tv</a></h3>
</div>
