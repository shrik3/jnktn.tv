---
streamer: Ruffy
streamer_id: ruffy
picture: https://files.mastodon.social/accounts/avatars/000/394/214/original/667b3703d6b69d11.jpg
country: 🇦🇹
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Placerat duis ultricies lacus sed turpis tincidunt id aliquet risus. 

<!-- more -->

# Ruffy

<img src="https://files.mastodon.social/accounts/avatars/000/394/214/original/667b3703d6b69d11.jpg" height="128" width="128" style="margin: 10px auto; display: block;" alt="ruffy"/>

| Facts        |             |
|:------------:|:-----------:|
| From         | Linz, AT 🇦🇹 |
| Pronouns     | he/him      |
| Joined Jnktn | 2021        |

## Intro

People usually refer to him as Ruffy (or Raffi for the German-speaking folks). He's approx. 0x1A years old and from a small Austrian city near Linz. He's into clean-code, with a strong preference for backend work. To counterbalance the time in front of the screen, he's also making music. He plays some instruments like the piano, clarinet, drums and a few more. He produces his own music, mostly in electronic genres (such as Hands Up, Hardstyle, House, Dance, Electro, Dubstep, Drum & Bass, Hardcore and much more).

## My favorite Jnktn Show

{% mixcloud_playlist dance-attack-with-ruffy %}

## Get in touch

- [Mastodon](https://mastodon.social/web/@rarepublic)
- [Ruffys Blog](https://blog.rtrace.io)
- [Soundcloud](https://soundcloud.com/djraremusic)