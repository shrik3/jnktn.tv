---
streamer: Jumboshrimp
streamer_id: jumboshrimp
picture: https://files.mastodon.social/cache/accounts/avatars/001/412/466/original/ca74eadc210f3d87.jpg
country: 🏴󠁧󠁢󠁳󠁣󠁴󠁿
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Placerat duis ultricies lacus sed turpis tincidunt id aliquet risus. 

<!-- more -->

# Jumboshrimp

<img src="https://files.mastodon.social/cache/accounts/avatars/001/412/466/original/ca74eadc210f3d87.jpg" height="128" width="128" style="margin: 10px auto; display: block;" alt="jumboshrimp"/>

| Facts        |              |
|:------------:|:------------:|
| Country      | Scotland 🏴󠁧󠁢󠁳󠁣󠁴󠁿  |
| Pronouns     | he/him       |
| Joined Jnktn | 2020         |

## Intro

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Placerat duis ultricies lacus sed turpis tincidunt id aliquet risus. Sed odio morbi quis commodo odio aenean. Egestas sed tempus urna et pharetra pharetra massa massa ultricies. Risus quis varius quam quisque id. A diam sollicitudin tempor id eu nisl nunc mi ipsum. Sed blandit libero volutpat sed cras ornare arcu dui vivamus. Senectus et netus et malesuada fames ac turpis egestas integer. Dui accumsan sit amet nulla facilisi. Est velit egestas dui id ornare. Erat imperdiet sed euismod nisi porta lorem mollis aliquam ut. Semper quis lectus nulla at volutpat diam. Sit amet commodo nulla facilisi. Feugiat in fermentum posuere urna. Id aliquet lectus proin nibh nisl condimentum id venenatis. Sem viverra aliquet eget sit amet tellus cras.

## Shows

{% mixcloud_playlist jumboshrimps-radio-show %}
{% mixcloud_playlist jumboshrimp %}

## Get in touch

- [Mastodon](https://mastodon.social/web/@jumboshrimp@fosstodon.org)