---
streamer: Michan
streamer_id: michan
---

Yet another grain of dust in an everlasting journey to recognize and admire the beauty.

<!-- more -->

# Michan

| Facts        |           |
|:------------:|:---------:|
| Pronouns     | he/him/it |
| Joined Jnktn | 2020      |

## Who?
Hi there! My username is Michan in the chat. I’m a [high functioning] music addict.

## What am I doing?
Each month I add a pinch of [my favourite spice mix](../shows/unpopularstream) to all the cool tunes people share on jnktn.tv. Also, from time to time I contribute to jnktn.tv’s infrastructure as well as the opensource tools used around here.

{% mixcloud_playlist unpopular-stream 3 %}

## Why here?
I understand that the context is crucial and spending a lot of time in weird places to find an obscure piece of music and listening to numerous uninspiring tunes in the process is an important part of the ritual for meeting a special one. But I also believe that there are people with curious imagination who can hear through all the eccentricity and appreciate the beauty to a great extent. Jnktn.tv is full of these people. A group of friends from different backgrounds who meet at least once a week right here! 

## What’s on my ears?
If I’m intently listening to something:
* Beyerdynamic DT1990 Pro (with SMSL SH-9, SU-9)
* Etymotic ER2SE (with Audioquest DragonFly Black)
* Audio Technica ATH-M50x

If it’s supposed to be mixed in the background of my life [regrettably!]:
* Sennheiser MOMENTUM Wireless M2 AEBT
* Sennheiser CX 686G
* KZ ZS10 Pro

##  What's playing?
From shitgaze to witch house, I listen to music in every genre but the most fascinating ones for me are those which don't follow the beaten path.

## Which Rammstein perfume?
All of them depending on my mood. But I think I'm reaching for the *Kokain Black Intense* more these days!

## Wh... [more questions]?
Drop me an email: "michan" [AT] whatever the name of this site was!
