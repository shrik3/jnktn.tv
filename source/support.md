# Support

Jnktn.tv does not run ads nor receives any external financial support, so we rely on donations to keep the project going. All acts on the platform join us on a voluntary basis and as such do not receive any payment.

Donations can be made via the services listed below. Both Liberapay and Ko-Fi deal with recurring donations, with Ko-Fi also accepting one-off payments. Anything you can spare to support us is greatly appreciated.

Merchandise is available, with us receiving a percentage of each sale.

All donations will go towards the following costs, with any surplus being saved for future months.

| Cost                                  | Amount          |
|:-------------------------------------:|----------------:|
| Virtual Private Server                | €13.5 per month |
| Chat subscription                     |  $2 per month   |
| Domain name                           |  £3 per month   |
| Total                                 | ~£16 per month  |


## Liberapay

Liberapay is an open-source and non-profit recurrent donations platform.

Compared to similar donation platforms like Patreon, Liberapay don't take a cut from your donations and they allow anonymous contributions.

<a href="https://liberapay.com/Jnktn.tv/donate" rel="me noopener" target='_blank'><img alt="Donate using Liberapay" src="./assets/icon/liberapay_donate.svg"></a>


## Ko-Fi

Ko-Fi is a donations platform that enabled both one-time and monthly support. As with Liberapay, Ko-Fi don't take a cut of your donations.

Donate the cost of a coffee to show your support!

<a href='https://ko-fi.com/U7U02CV3O' rel="me noopener" target='_blank'><img class="kofi_donate" alt='Buy Me a Coffee at ko-fi.com' src='./assets/icon/kofi1.png'></a>


## Merchandise

Teemill is our merchandise provider. They are a sustainable organisation that produce items from their renewable energy powered factory. Also, their website is carbon neutral and they don't use plastic in their packaging.

Check out our offerings and let us know if you would like other colours or items added to the range.

<h3 class="link"><a href="https://jnktn.teemill.com" rel="me noopener" target='_blank'>Jnktn.tv Merchandise</a></h3>
