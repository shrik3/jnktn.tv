---
title: JCs Juice Box
streamer_id: jc
show_id: jcsjuicebox
---

Nothing but the finest, freshest squeezed retro electronic juice straight from the nips of JC! What flavour will we get this time?

<!-- more -->

# [JCs](/streamers/jc) Juice Box

**Hosted by: [JC](/streamers/jc)**

![JCs Juice Box](/assets/img/shows/jcsjuicebox.png)

Nothing but the finest, freshest squeezed retro electronic juice straight from the nips of JC! What flavour will we get this time? Form an orderly queue when the box's start getting handed out, always guaranteed to deliver a strong dose of juicy beats & visuals.


![little cute goat](/assets/img/memes/goat.png)
*Please be advised, goats are harmed in the production of this show


## What's being played?

While JC's ready to serve any flavour he's in the mood for, you can always expect a healthy glug of;

- Techno
- Trance
- Progressive

## Shows

{% mixcloud_playlist jcs-juice-box %}
