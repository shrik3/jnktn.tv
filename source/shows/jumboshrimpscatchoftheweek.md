---
title: Jumboshrimp's catch of the week
streamer_id: jumboshrimp
show_id: jumboshrimpscatchoftheweek
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Placerat duis ultricies lacus sed turpis tincidunt id aliquet risus. 

<!-- more -->

# [Jumboshrimps](/streamers/jumboshrimp) Catch of the week

**Hosted by: [Jumboshrimp](/streamers/jumboshrimp)**

## What's being played?

- Trance
- Garage
- Future
- Rock & Pop
- House & Electro
