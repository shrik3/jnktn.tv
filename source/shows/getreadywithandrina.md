---
title: Get Ready with Andrina
streamer_id: andrina
show_id: getreadywithandrina
---

Grab your make up, dancing shoes and Prosecco as you get ready for your night in or out as Andrina takes you through skin care and make up routines whilst jamming to amazing beats.

<!-- more -->

# Get Ready with [Andrina](/streamers/andrina)

**Hosted by: [Andrina](/streamers/andrina)**

![Get ready with Andrina](/assets/img/shows/getreadywithandrina.png)

Grab your make up, dancing shoes and Prosecco as you get ready for your night in or out as Andrina takes you through skin care and make up routines whilst jamming to amazing beats.

## What's being played?

- 80s/90s Dance
- House & Electro
- Hip Hop
- Rock
- Pop

## Shows

{% mixcloud_playlist get-ready-with-andrina %}
