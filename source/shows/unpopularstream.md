---
title: Unpopular Stream
streamer_id: michan
show_id: unpopularstream
---

A selection from Michan’s favourite tracks for night-time strolls. It’s an hour-long playlist, mostly from non-mainstream artists and includes every genre from splittercore to dark ambient, accompanied with a city walk video. 

<!-- more -->

# Unpopular Stream

**Hosted by: [Michan](/streamers/michan)**

Have you ever thought: what is music and what is not? where are all these genres coming from? Or, what if audience’s financial support was not a determining factor in artists’ decisions? To wonder about these questions and many more, join us in our next adventure of Unpopular Stream. Bring good headphones for the best experience!

## Origins
Unpopular Stream is a tribute to **Øfdream**. An artist who inspired a generation. 

>  “May the legacy of Øfdream live forever.”

{% soundcloud playlist 940150522 %}

## Archive

{% mixcloud_playlist unpopular-stream 4 %}

Checkout our [Mixcloud playlist](https://www.mixcloud.com/Jnktn_TV/playlists/unpopular-stream/) for the complete list of past shows.
