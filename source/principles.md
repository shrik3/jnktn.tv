# Our Core Principles

* **Diversity and Inclusiveness**: We are against any kind of discrimination. We strive to be an inclusive platform catering the needs of a diverse group of audience.
* **Privacy Respecting**: We believe the right to online privacy is an indisputable part of human rights.
 * We are entirely funded by voluntary donations. We don’t gather and sell user data for financial gains. We don’t use cookies, or any kind of tracking technology. And we anonymize all our server logs which are kept for a short time and used only for monitoring our service quality. 
 * We encourage the engagement through privacy respecting services e.g. Mastodon, Matrix, etc.
* **Open collaboration**: [*add a catchy sentence for why do open source!*]
 * We use open source software as much as possible in all layers of our infrastructure. This includes the broadcasting tools, video storage and streaming servers, webserver, git hosting, donation platform, etc.
 * We regularly contribute to the projects we benefit from, by financially supporting them and/or collaborating to their development.
 * We release all our internally developed codes under a free license for everybody to use. 

