## Jnktn.tv

![Jnktn.tv Logo](/themes/jnktn-main/source/assets/img/jnktn_logo_blackonyellow_border.png)

Welcome to the [Jnktn.tv](https://jnktn.tv) repository. Here you'll find the code for our website. Jnktn.tv is powered by [Owncast](https://github.com/owncast/owncast) and [Cbox Live Chat](https://www.cbox.ws).

If you'd like to help out with the development, PRs are welcome.

## Read further

- [Details about how to develop/work on the Jnktn website](docs/site.md)
