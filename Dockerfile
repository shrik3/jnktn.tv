# > Build Stage
# - installs all the node build tools required to generated the website
# - installs the dependencies of the static website (e.g. hexo)
# - generates the static website and stores the output in /jnktntvsite/public
# --------------------------------------------------------------------------------
FROM node:alpine as build_stage

WORKDIR /jnktntvsite
RUN echo "Installing hexo-cli" && npm install hexo hexo-cli -g

COPY package.json package-lock.json ./
RUN echo "Installing node Dependencies" && npm install

COPY . .
RUN echo "Generating Static Site" && hexo generate

# > Actual Image
# - keep container image lightweight
# - no further dependencies other than nginx and the static site itself
# --------------------------------------------------------------------------------
FROM fedora:latest

LABEL org.opencontainers.image.title="Jnktn.tv Website"
LABEL org.opencontainers.image.description="The static website for http://jnktn.tv"
LABEL org.opencontainers.image.url="https://jnktn.tv"
LABEL org.opencontainers.image.authors="info@jnktn.tv"
LABEL org.opencontainers.image.source="https://codeberg.org/jnktn.tv/jnktn.tv"
LABEL org.opencontainers.image.documentation="https://codeberg.org/jnktn.tv/jnktn.tv"

EXPOSE 80

RUN dnf up -y && dnf install nginx -y
COPY --from=build_stage /jnktntvsite/public/ /var/www/jnktn
COPY /config/nginx.conf /etc/nginx/nginx.conf
COPY /config/jnktn.nginx.conf /etc/nginx/conf.d/jnktn.nginx.conf

CMD ["nginx", "-g", "daemon off;"]
