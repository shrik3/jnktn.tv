const axios = require("axios");

hexo.extend.tag.register("mixcloud_playlist", async function(args){
    const playlistId = args[0];
    const maxItems = Number(args[1]);
    const apiUrl = `https://api.mixcloud.com/Jnktn_TV/playlists/${playlistId}/cloudcasts`;

    return axios.get(apiUrl).then(function(response) {
        return response.data['data']
            .map(entry => entry.url)
            .map(embedId => `<iframe class="mixcloud-embed" width="100%" height="120" src="https://www.mixcloud.com/widget/iframe/?hide_cover=1&feed=${encodeURIComponent(embedId)}" frameborder="0"></iframe>`)
            .slice(0, maxItems)
            .join(''); 
    });
    
  }, { async: true });
