hexo.extend.tag.register('mixcloud', function(args, content) {
    const src = args[0]
        .replace('http://www.mixcloud.com', '')
        .replace('https://www.mixcloud.com', '')
        .replace('www.mixcloud.com')
        .replace('mixcloud.com')

    return `<iframe class="mixcloud-embed" width="100%" height="120" src="https://www.mixcloud.com/widget/iframe/?hide_cover=1&feed=${encodeURIComponent(src)}" frameborder="0"></iframe>`;
});
