hexo.extend.tag.register('soundcloud', function(args, content) {
    var type = args[0];
    var id = args[1];

    if(type === 'track')
        return `<div class="soundcloud-embed"><iframe height="240" class="soundcloud-embed" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/${id}&color=%230000ff&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe></div>`;

    if(type === 'playlist')
        return `<div class="soundcloud-embed"><iframe height="240" class="soundcloud-embed" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/${id}&color=%230000ff&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe></div>`

    return '';
});
